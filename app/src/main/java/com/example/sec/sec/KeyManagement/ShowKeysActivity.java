package com.example.sec.sec.KeyManagement;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.sec.sec.R;


public class ShowKeysActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showkeys);

    }
    public void genKey(View view) {
        Context context;
        context = getApplicationContext();
        Intent intent = new Intent(context, GenerateKeyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void keyList(View view) {
        Context context;
        context = getApplicationContext();
        Intent intent = new Intent(context, ListKeys.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }
}
